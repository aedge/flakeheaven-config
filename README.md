Put in your `pyproject.toml`:

```toml
[tool.flakeheaven]
base = ["https://gitlab.com/aedge/flakeheaven-config/-/raw/main/flakeheaven.toml"]
baseline = ".flakeheaven-baseline"

# <-- any overrides for flakeheaven go here

[tool.mypy]
plugins = "pydantic.mypy,sqlmypy"

[tool.isort]
multi_line_output = 3
include_trailing_comma = true
use_parentheses = true

[tool.pydocstyle]
convention = "google"
```

If you have more that a few overrides, you might want to load them from a separate file, we usually call it `.flakeheaven.toml`:

```toml
[tool.flakeheaven]
base = [
    "https://gitlab.com/aedge/flakeheaven-config/-/raw/main/flakeheaven.toml",
    ".flakeheaven.toml"
]
# ...
```
